﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolPlayerController : MonoBehaviour {


    public GameObject player;
    public Rigidbody2D playerRB;
    public SwimmingPoolSceneController sceneController;
    public float speed;

   // public 

    private bool isRightPressed;
    private bool isLeftPressed;
    public bool isStarted;
    private Vector3 tryMove;

    // Use this for initialization
    void Start () {
        isRightPressed = false;
        isLeftPressed = false;
        isStarted = false;
    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log("Player " + isStarted);
        if(isStarted) {
            Debug.Log("Player player");
            tryMove = Vector3.zero;
            tryMove += Vector3.left;
            if (isRightPressed) {
                tryMove += Vector3Int.up;
            } else if (isLeftPressed) {
                tryMove += Vector3Int.down;
            }
            playerRB.velocity = Vector3.ClampMagnitude(tryMove, 1f) * speed;
        } else {
            playerRB.velocity = Vector3.ClampMagnitude(new Vector3(0f, 0f, 0f), 1f) * 0;
        }
    }

    void OnTriggerEnter2D(Collider2D col) {
        Debug.Log("OnTriggerEnter");

        if(col.gameObject.name == "FinishLine") {
            // finish line
            //
            Debug.Log("Finish line !!!!!");
            SoundManager.instance.PlaySound("Success");
            sceneController.Stepper();
            isStarted = false;
        }

 /*       if (col.gameObject.tag == "AlertCollider" || col.gameObject.tag == "Wall") {
            if(col.gameObject.transform.position.y > player.transform.position.y){
                Debug.Log("LEFT !");
                SoundManager.instance.PlaySound("RightAlert");
            } else {
               SoundManager.instance.PlaySound("LeftAlert");
            }
        }*/

        if (col.gameObject.tag == "InflatableCollider") {
            isStarted = false;
            sceneController.ShowInflatableMessage();
            // send player to start point
        }

        if(col.gameObject.name == "GoldenCup") {
            SoundManager.instance.PlaySound("Success");
            TimeController.instance.KnockSeconds(5);
            sceneController.Stepper();
            isStarted = false;
        }
        if(col.gameObject.name == "SilverCup") {
            SoundManager.instance.PlaySound("Success");
            TimeController.instance.KnockSeconds(5);
            sceneController.Stepper();
            isStarted = false;
        }
        if(col.gameObject.name == "BronzeCup") {
            SoundManager.instance.PlaySound("Success");
            TimeController.instance.KnockSeconds(5);
            sceneController.Stepper();
            isStarted = false;
        }
    }


    public void GetPlayerToScene() {
        player.SetActive(true);

    }

    public void ReStartGame() {
        player.transform.position = new Vector3(7.61f, 0f, 0f);
        isStarted = true;
    }

    #region player_controls
    //*** Player Control Buttons Events


    public void RightButtonPressed() {
        Debug.Log("RBP");
        isRightPressed = !isLeftPressed;
    }

    public void LeftButtonPressed() {
        Debug.Log("LBP");
        isLeftPressed = !isRightPressed;
    }

    public void RightButtonUp() {
        isRightPressed = false;
    }

    public void LeftButtonUp() {
        isLeftPressed = false;
    }
    #endregion
}
