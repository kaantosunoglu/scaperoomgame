﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class MainSceneController : MonoBehaviour {

    public VideoPlayer vp;
    public GameObject startButton;
    public GameObject twoPlayerSelectButton;
    public GameObject fourPlayerSelectButton;

    public GameObject readyPanel;
    public GameObject twoPlayerReadyPanel;
    public GameObject touchToStartButton;
    public Button RButton1;
    public Button RButton2;
    public Button RButton3;
    public Button RButton4;
    public Text Rtext1;
    public Text Rtext2; 
    public Text Rtext3;
    public Text Rtext4;

    private int readyPlayer = 0;

    private int step = 0;
    private bool changeStep = false;
    private int playerCount;
    int touchCounter = 0;

    // Use this for initialization
    void Start() {
        SoundManager.instance.PlaySound("MainTheme");
    }

    void Update() {
        if (Input.GetMouseButtonDown(0))
        {
            touchCounter++;
            if (touchCounter == 2)
            {
                touchCounter = 0;
                vp.frame = (long)vp.frameCount;
            }
        }
    }

    // Go to Safari Game Scene
    public void StartPlaying(){
        changeStep = true;
        startButton.SetActive(false);
    }

    public void StartVideoSequence()
    {
        touchToStartButton.GetComponent<Button>().Select();
        startButton.SetActive(false);
        vp.isLooping = false;
        vp.loopPointReached += PlayFirstVideo;
    }

    //Play first video after looping intro
    void PlayFirstVideo(VideoPlayer vp)
    {
        touchToStartButton.SetActive(false);
        vp.loopPointReached -= PlayFirstVideo;
        vp.isLooping = false;
        vp = VideoManager.instance.PlayVideo("1");
        vp.loopPointReached += PlaySecondLoopVideo;
    }

    //This is when you choose 2 or 4 players
    void PlaySecondLoopVideo(VideoPlayer vp)
    {
        twoPlayerSelectButton.SetActive(true);
        fourPlayerSelectButton.SetActive(true);
        vp.loopPointReached -= PlaySecondLoopVideo;
        vp = VideoManager.instance.PlayVideo("2");
        vp.isLooping = true;
    }

    //Video after you've choosen players
    void PlayThirdVideo(VideoPlayer vp)
    {
        vp.loopPointReached -= PlayThirdVideo;
        vp.loopPointReached += PlayFourthLoopVideo;
        twoPlayerSelectButton.SetActive(false);
        fourPlayerSelectButton.SetActive(false);
    }

    //When you press ready to play buttons
    void PlayFourthLoopVideo(VideoPlayer vp)
    {
        vp = playerCount == 2 ? VideoManager.instance.PlayVideo("4-2") : VideoManager.instance.PlayVideo("4-4");
        vp.isLooping = true;
        if(playerCount == 2)
        {
            twoPlayerReadyPanel.SetActive(true);
        }
        else
        {
            readyPanel.SetActive(true);
        }
    }

    public void PlayerCountSelected(int count) {
        TimeController.instance.playerCount = count;
        playerCount = count;
        vp.isLooping = false;
        AnalyticsManager.instance.GameStart(count);
        vp = playerCount == 2 ? VideoManager.instance.PlayVideo("3-2") : VideoManager.instance.PlayVideo("3-4");
        vp.prepareCompleted += PlayThirdVideo;
    }

    public void GotoSwim(){
        SceneManager.LoadScene("Pool", LoadSceneMode.Single);
        SoundManager.instance.StopSound("MainTheme");
    }

    private void OpenSafariScene(VideoPlayer vp) {
        vp.loopPointReached -= OpenSafariScene;
        vp = VideoManager.instance.PlayVideo("savannahVideo");
        vp.loopPointReached += ChangeScene;
        SoundManager.instance.StopSound("MainTheme");
        SoundManager.instance.PlaySound("SafariTheme");
       
       // StartCoroutine(WaitForNextScene(3f));
    }

    void CheckIfAllPlayersReady()
    {
        if (playerCount == 2 && readyPlayer > 1)
        {
            twoPlayerReadyPanel.SetActive(false);
            vp = VideoManager.instance.PlayVideo("5-2");
            vp.loopPointReached += OpenSafariScene;
        }
        if (playerCount == 4 && readyPlayer > 3)
        {
            readyPanel.SetActive(false);
            vp = VideoManager.instance.PlayVideo("5-4");
            vp.loopPointReached += OpenSafariScene;
        }
    }

    public void PlayerReady1(){
        RButton1.interactable = false;
        readyPlayer++;
        CheckIfAllPlayersReady();
    }

    public void PlayerReady2() {
        RButton2.interactable = false;
        readyPlayer++;
        CheckIfAllPlayersReady();
    }


    public void PlayerReady3() {
        RButton3.interactable = false;
        readyPlayer++;
        CheckIfAllPlayersReady();
    }

    public void PlayerReady4() {
        RButton4.interactable = false;
        readyPlayer++;
        CheckIfAllPlayersReady();
    }

    IEnumerator WaitForNextScene(float m) {
        yield return new WaitForSeconds(m);
         SceneManager.LoadScene("Safari", LoadSceneMode.Single);
        //SceneManager.LoadScene("Pool", LoadSceneMode.Single);
    }
    private void ChangeScene(VideoPlayer vp){
        SceneManager.LoadScene("Safari", LoadSceneMode.Single);
    }
}
