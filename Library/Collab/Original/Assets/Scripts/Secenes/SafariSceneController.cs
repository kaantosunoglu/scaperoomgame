﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class SafariSceneController : MonoBehaviour {


    public GameObject player;
    public GameObject buttonSetFor2;
    public GameObject buttonSetFor4;
    public RectTransform avatar;
    public RectTransform finishAvatar;
    public RectTransform[] bubbles;
    public Text[] textAreas;
    public Text[] codeTexts;
    public Image[] codeTextsImages;
    public Button[] codeButtons;
    public GameObject[] rhinos;
    public GameObject[] animals;
    public GameObject proacher;
    public Button backButton;
    public RectTransform codePanel;
    public GameObject codeKeyboard;
    public GameObject outroPanel;
    public VideoPlayer vp;
    public VideoPlayer swimVideo;
    public VideoPlayer gameEndVideo;
    public GameObject challengeCompletePanel;
    //public GameObject nextChallengePanel;
    public Text timeText;
  //  public GameObject swimpool;

    public Sprite codeButtonSprite;
    public Sprite errorButtonSprite;
    public Sprite correctCodeSprite;
    public Sprite errorCodeSprite;
    public Sprite CodeSprite;
    public Sprite bubbleSprite;
    public Sprite errorBubbleSprite;
    public Sprite backButtonSprite;
    public Sprite backButtonErrorSprite;

    public GameObject backPanel;

    public GameObject goPanel;
    public Text goText1;
    public Text goText2;

    public GameObject twoPlayerPanel;

    private string[] texts;

    private int startTime;

    private int step;
    private int bubbleStep;
    private int codeStep;
    private bool isVideoPlaying = false;
    private int codeErrorStep;
    private int goStep;
    private bool isPoacherIn;
    private int taggedAnimals;
    private bool isProacherCatched;

    // Use this for initialization
    void Start() {
        goPanel.SetActive(false);
        outroPanel.SetActive(false);
        twoPlayerPanel.SetActive(false);
        startTime = 0;
        taggedAnimals = 0;
        vp.loopPointReached += VideoStepper;
        swimVideo.loopPointReached += VideoStepper;
        gameEndVideo.loopPointReached += VideoStepper;
        step = -1;
        bubbleStep = 0;
        codeStep = 0;
        codeErrorStep = -1;
        goStep = 4;
        isProacherCatched = false;
        texts = new string[6];
        texts[0] = "Hi Game Warden\nWelcome to the game reserve.\nWe need your help to protect the endangered Rhinos.";
        texts[1] = "First you need to find a clue.\nMaybe, your uniform will hold the key?";
        texts[2] = "That’s it! Let’s see what you’ve unlocked…";
        texts[3] = "Now it's your turn\nYou have to find and stop the poacher, before it’s too late!";
        texts[4] = "Find the animals with your jeep and tag them. The more you tag, the easier it will be to catch the poacher!";
        texts[5] = "You’ve done it!\nYou’ve helped save the Rhino from a deadly threat!";

        for (int i = 0; i < bubbles.Length; i++) {
            LeanTween.alpha(bubbles[i], 0, 0f);
        }
        LeanTween.alpha(avatar, 0, 0f);
        LeanTween.alpha(finishAvatar, 0, 0f);
        LeanTween.alpha(codePanel, 0, 0f);
        LeanTween.alpha(proacher, 0f, 0f);
        if (TimeController.instance.playerCount == 4) {
            PlayersAreReady();
        } else {
            //twoPlayerPanel.SetActive(true);
            PlayersAreReady();
        }
        isPoacherIn = false;

       

    }

    // Update is called once per frame
    void Update() {
    }

    public void PlayersAreReady() {
        twoPlayerPanel.SetActive(false);
        backPanel.SetActive(true);
        AvatarInAnimation();
        Stepper();
    }


    private void goAnim(){
        goStep--;
        if(goStep>0) {
            goText1.text = goStep.ToString();
            goText2.text = goStep.ToString();
            LeanTween.alpha(goPanel, 1, 1).setOnComplete(goAnim);
        } else if(goStep == 0) {
            goText1.text = "GO!";
            goText2.text = "GO!";
            LeanTween.alpha(goPanel, 1, 1).setOnComplete(goAnim);
        } else {
            goPanel.SetActive(false);
            Stepper();
        }

    }

    /**** Safari game steps
        0 - First bubble and texts
        1 - Wait for reading
        2 - First bubbles out
        3 - Second bubble and texts / 
        4 - Open code panel
        5 - After entered correct code close code panel and Second Bubble
        6 - Open Third Bubble
        7 - Wait for reading
        8 - Video
        9 - Close third bubble
        10 - Fourth bubble and text
        11 - Jeep comes and wait for jeep
        12 - Wair for reading
        13 - Close fourth bubble / Fade out avatar
        14 - Open game control panel
        15 - Close game control panel and open avatar after finished game
        16 - Open sixth bubble and text
        17 - 
        18 - 
        19 - 

    ***/
    public void Stepper() {
        step++;
        Debug.Log("step " + step);

        switch (step) {
            case 0:
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("SAFARI_SB_1");
                break;
            case 1:
                StartCoroutine(WaitForNextStep(5f));

                break;
            case 2:
                BubbleOutAnimation(0);
                break;
            case 3:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("SAFARI_SB_2");

                break;
            case 4:

                OpenCodePanel();
                break;
            case 5:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                WriteBubbleText("SAFARI_SB_4");
                break;
            case 6:
                StartCoroutine(WaitForNextStep(3f));
                break;
            case 7:
                CloseCodePanel();
                BubbleOutAnimation(1);
                SoundManager.instance.PauseSound("SafariTheme");
                break;
            case 8:
                isVideoPlaying = true;
                vp.Play();
                break;
            case 9:
                BubbleOutAnimation(2);
                vp.Stop();
                SoundManager.instance.ResumeSound("SafariTheme");
                TimeController.instance.homeButton.SetActive(true);
                TimeController.instance.timerPannel.SetActive(true);
                SoundManager.instance.PlaySound("jeepStart");
                player.GetComponent<SafariPlayerController>().GetPlayerToScene();
                break;
            case 10:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("SAFARI_SB_5");
                break;
            case 11:
                SoundManager.instance.PlaySound("jeep");
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 12:
                BubbleOutAnimation(3);
                break;
            case 13:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("SAFARI_SB_6");
                break;
            case 14:
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 15:
                BubbleOutAnimation(4);
                break;
            case 16:
                AvatarOutAnimation();
                break;
            case 17:
                goPanel.SetActive(true);
                goAnim();
                break;
            case 18:
                OpenControlPanel();
                TimeController.instance.StartTimer(99999);
                break;
            case 19:

                TimeController.instance.PauseTimer();
                backPanel.SetActive(false);
                CloseControlPanel();
                FinishAvatarInAnimation();
                break;
            case 20:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("SAFARI_SB_7");
                break;
            case 21:
                for (int i = 0; i < animals.Length; i++) {
                    animals[i].SetActive(false);
                }
                for (int i = 0; i < rhinos.Length; i++) {
                    rhinos[i].GetComponent<AnimalTagControl>().Shining();
                }
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 22:

                StartCoroutine(WaitForNextStep(3f));
              
                break;
            case 23:
                TimeController.instance.homeButton.SetActive(false);
                TimeController.instance.timerPannel.SetActive(false);
                outroPanel.SetActive(true);
                challengeCompletePanel.SetActive(true);
                timeText.text = TimeController.instance.getFormattedGameTimeText(TimeController.instance.totalSeconds - startTime);
                break;
            case 24:
                challengeCompletePanel.SetActive(false);
               // nextChallengePanel.SetActive(true);
                gameEndVideo.Play();
                //StartCoroutine(WaitForNextStep(3f));
                break;
            case 25:
                SoundManager.instance.StopSound("SafariTheme");
                SoundManager.instance.StopSound("jeep");
                SoundManager.instance.PlaySound("SwimmerTheme");
                TimeController.instance.timerPannel.SetActive(false);
                gameEndVideo.Stop();
                //nextChallengePanel.SetActive(false);
                // swimpool.SetActive(true);
                swimVideo.Play();
                //StartCoroutine(WaitForNextStep(3f));
                break;
            case 26:
                SceneManager.LoadScene("Pool", LoadSceneMode.Single);
                break;
            default:
                break;
        }

    }

    private void BubbleInAnimation(string sound) {
        Debug.Log(bubbleStep);
        Vector3 bubblePosition = bubbles[bubbleStep].anchoredPosition;
        Debug.Log(bubblePosition);

        LeanTween.move(bubbles[bubbleStep], new Vector3(bubblePosition.x, bubblePosition.y + 10f, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(WriteBubbleText).setOnCompleteParam(sound);
        LeanTween.alpha(bubbles[bubbleStep], 1, 0.5f);
    }

    private void BubbleOutAnimation(int b) {
        Vector3 bubblePosition = bubbles[b].anchoredPosition;

        LeanTween.move(bubbles[b], new Vector3(bubblePosition.x, bubblePosition.y + 10f, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(Stepper);
        LeanTween.alpha(bubbles[b], 0, 0.5f);
    }

    private void AvatarInAnimation() {
        LeanTween.alpha(avatar, 1, 0.5f);
    }

    private void AvatarOutAnimation() {
        LeanTween.alpha(avatar, 0, 0.5f).setOnComplete(Stepper);
    }

    private void FinishAvatarInAnimation() {
        LeanTween.alpha(avatar, 1, 0.5f).setOnComplete(Stepper);
    }

    private void FinishAvatarOutAnimation() {
        LeanTween.alpha(avatar, 0, 0.5f).setOnComplete(Stepper);
    }

    private void OpenCodePanel() {
        LeanTween.alpha(codePanel, 1, 0.5f);
    }

    private void CloseCodePanel() {
        LeanTween.alpha(codePanel, 0, 0.5f).setOnComplete(DeactiveControlPanel);
        // Stepper();
    }

    private void DeactiveControlPanel() {
        codeKeyboard.SetActive(false);
    }

    private void OpenControlPanel() {
        //  TimeController.instance.StartTimer(120);
        if (TimeController.instance.playerCount == 4){
            buttonSetFor4.SetActive(true);
        }else{
            buttonSetFor2.SetActive(true);
        }
    }

    private void CloseControlPanel() {
        buttonSetFor4.SetActive(false);
    }

    private void WriteBubbleText(object sound) {
        SoundManager.instance.PlaySound(sound.ToString());
        StartCoroutine(WriteText(
            textAreas[bubbleStep],
            texts[bubbleStep]
        ));
    }

    public void WriteCode(string c) {


        if(c == "B") {
            if (codeStep > 0) {
                codeStep--;
                codeTexts[codeStep].text = "";
            }
            return;
        }

        Debug.Log(codeStep);
        SoundManager.instance.PlaySound("select");
        codeTexts[codeStep].text = c;
        codeStep++;
        if (codeStep == 4 && CheckCode()) {
            SoundManager.instance.PlaySound("correct");
            codeStep = 0;
            for (int i = 0; i < codeTexts.Length; i++) {
                codeTextsImages[i].sprite = correctCodeSprite;
            }
            Stepper();
        } else if (codeStep == 4 && !CheckCode()) {
            SoundManager.instance.PlaySound("incorrect");
            codeStep = 0;
            bubbles[bubbleStep].GetComponent<Image>().sprite = errorBubbleSprite;
            for (int i = 0; i < codeButtons.Length; i++) {
                codeButtons[i].interactable = false;
                codeButtons[i].GetComponent<Image>().sprite = errorButtonSprite;
            }
            backButton.interactable = false;
            backButton.GetComponent<Image>().sprite = backButtonErrorSprite;
            for (int i = 0; i < codeTexts.Length; i++) {
                codeTextsImages[i].sprite = errorCodeSprite;
            }
            codeErrorStep = -1;
            CodeErrorStepper();

        }
    }

    private bool CheckCode() {
        string coded = "";
        for (int i = 0; i < codeTexts.Length; i++) {
            coded += codeTexts[i].text;
        }

        return (coded.Length == 4 && coded == "3247");
    }

    private void ClearCode() {

        for (int i = 0; i < codeTexts.Length; i++) {
            codeTextsImages[i].sprite = CodeSprite;
            codeTexts[i].text = "";
        }
        CodeErrorStepper();
    }

    IEnumerator WriteText(Text textArea, string text) {
        string currentText = "";
        for (int i = 0; i < text.Length + 1; i++) {
            currentText = text.Substring(0, i);
            textArea.text = currentText;
            yield return new WaitForSeconds(0.07f);
        }
        Stepper();
    }


    // using just for wrong code
    IEnumerator WriteText(string t) {
        string text = t;
        string currentText = "";
        textAreas[bubbleStep].text = "";
        for (int i = 0; i < text.Length + 1; i++) {
            currentText = text.Substring(0, i);
            textAreas[bubbleStep].text = currentText;
            yield return new WaitForSeconds(0.07f);
        }

        CodeErrorStepper();
        // Stepper();
    }

    IEnumerator WaitForNextStep(float m) {
        yield return new WaitForSeconds(m);
        Stepper();
    }
    IEnumerator WaitForNextErrorStep(float m) {
        yield return new WaitForSeconds(m);
        CodeErrorStepper();
    }


    private void VideoStepper(VideoPlayer vp) {
        Stepper();
    }

    private void CodeErrorStepper() {
        codeErrorStep++;
        switch (codeErrorStep) {
            case 0:
                SoundManager.instance.PlaySound("SAFARI_SB_3");
                StartCoroutine(WriteText("Unlucky! Try again!"));
                break;
            case 1:
                StartCoroutine(WaitForNextErrorStep(3f));
                break;
            case 2:
                ClearCode();
                break;
            case 3:
                bubbles[bubbleStep].GetComponent<Image>().sprite = bubbleSprite;
                for (int i = 0; i < codeButtons.Length; i++) {
                    codeButtons[i].GetComponent<Image>().sprite = codeButtonSprite;
                }
                backButton.GetComponent<Image>().sprite = backButtonSprite;
                StartCoroutine(WriteText(texts[1]));
                break;
            case 4:
                for (int i = 0; i < codeButtons.Length; i++) {
                    codeButtons[i].interactable = true;
                }
                backButton.interactable = false;
                break;
            default:
                break;
        }
    }

    public void PoacherInAnim() {
        if(!isPoacherIn) {
            LeanTween.alpha(proacher, 1f, .5f);
            LeanTween.moveX(proacher, 7.5f, 1.2f);
            isPoacherIn = true;
        }
    }

    public void CatchProacher() {

        proacher.SetActive(false);
    }

    public void AnimalTagged() {
        taggedAnimals++;
        Debug.Log("taggedAnimals " + taggedAnimals);
        if(taggedAnimals == 8) {
            PoacherInAnim();
        }
    }
    private void PoacherOutAnim() {
        LeanTween.alpha(proacher, 1f, .5f).setDelay(5.7f);
        LeanTween.moveX(proacher, 10f, 1.4f).setDelay(5f);
    }

}
