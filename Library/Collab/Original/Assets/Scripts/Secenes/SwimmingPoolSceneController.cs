﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class SwimmingPoolSceneController : MonoBehaviour {

    public GameObject buttonSet;
    public GameObject player;

    public RectTransform[] bubbles;
    public Text[] textAreas;
    public GameObject[] inflatables;
    public GameObject[] cups;
    public RectTransform avatar;

    public VideoPlayer vp;
    public VideoPlayer NHSVideo;
    public VideoPlayer gameEndVideo;
    public GameObject mask;
    public GameObject challengeCompletePanel;
    //public GameObject nextChallengePanel;
    public GameObject outroPanel;
   // public GameObject nhsimage;
    public Text timeText;

    public GameObject backPanel;
    public GameObject hitBackPanel;

    public GameObject goPanel;
    public Text goText1;
    public Text goText2;
    public GameObject twoPlayerPanel;
    public bool isGameStarted;



    private string[] texts;
    private int step;
    private int bubbleStep;

    private int startTime;
    private int goStep;


    // Use this for initialization
    void Start() {
        startTime = TimeController.instance.totalSeconds;
        SoundManager.instance.PauseSound("SwimmerTheme");
        hitBackPanel.SetActive(false);
        mask.SetActive(true);
        buttonSet.SetActive(false);
        player.SetActive(false);
        outroPanel.SetActive(false);
        backPanel.SetActive(true);
        goPanel.SetActive(false);
        twoPlayerPanel.SetActive(false);
        step = -1;
        bubbleStep = 0;
        goStep = 4;
        texts = new string[6];
        texts[0] = "OK Swimmer,\nWelcome to the pool.";
        texts[1] = "Would you dare to swim in the dark?\nLet's find out.";
        texts[2] = "Get your friends to guide you through the dark.\nListen to their voices and you’ll make it to the finish line.";
        texts[3] = "Try and avoid the inflatables! You’ll lose valuable time if you hit them.\nRight.It’s now time to put your goggles on. Dark isn’t it? ";
        texts[4] = "Well done. You made it!\nYou swam in the dark! ";
        texts[5] = "Careful, try and avoid the obstacles!\nDon’t forget, follow the guidance from your friends to the finish line.";

        for (int i = 0; i < bubbles.Length; i++) {
            LeanTween.alpha(bubbles[i], 0, 0f);
        }
        LeanTween.alpha(avatar, 0, 0f);


        if (TimeController.instance.playerCount == 4) {
            PlayersAreReady();
        } else {
            twoPlayerPanel.SetActive(true);
        }

        player.SetActive(false);
        vp.loopPointReached += VideoStepper;
        NHSVideo.loopPointReached += VideoStepper;
        gameEndVideo.loopPointReached += VideoStepper;

        //Stepper();
    }

    // Update is called once per frame
    void Update() {

    }

    public void PlayersAreReady() {
        twoPlayerPanel.SetActive(false);
        vp.Play();
    }


    private void goAnim() {
        goStep--;
        if (goStep > 0) {
            goText1.text = goStep.ToString();
            goText2.text = goStep.ToString();
            LeanTween.alpha(goPanel, 1, 1).setOnComplete(goAnim);
        } else if (goStep == 0) {
            goText1.text = "GO!";
            goText2.text = "GO!";
            LeanTween.alpha(goPanel, 1, 1).setOnComplete(goAnim);
        } else {
            goPanel.SetActive(false);
            Stepper();
        }

    }

    /**** Safari game steps
        0 - Video
        1 - First bubble and texts
        2 - Wait for reading
        3 - First bubbles out
        4 - Second bubble and text
        5 - Wait for reading
        6 - Second bubble out
        7 - Third bubble and text 2
        8 - Sound
        9 - add text 3
        10 - sound
        11 - add text 4
        12 - Wait for reading
        13 - Fourth bubble out
        14 - Fifth bubble and text / add player to scene
        15 - Wait for reading
        16 - Fifth bubble out / 
        17 - Avatar out
        18 - open controller / get player
        19 - Open sixth bubble and text

    ***/
    public void Stepper() {
        step++;
        Debug.Log("step " + step);

        switch (step) {

            case 0:
                player.GetComponent<PoolPlayerController>().GetPlayerToScene();
                AvatarInAnimation();
                SoundManager.instance.ResumeSound("SwimmerTheme");
                mask.SetActive(false);
                vp.Stop();
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("SWIMMING_SB_1");
                break;
            case 1:
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 2:
                BubbleOutAnimation(0);
                break;
            case 3:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("SWIMMING_SB_2");
                break;
            case 4:
                //ActiveInflatables();
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 5:
                BubbleOutAnimation(1);
                break;
            case 6:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("SWIMMING_SB_3");
                break;
            case 7:
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 8:
                BubbleOutAnimation(2);
                break;
            case 9:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("SWIMMING_SB_4");

                break;
            case 10:
                StartCoroutine(WaitForNextStep(3f));
                break;
            case 11:
                BubbleOutAnimation(3);
                break;
            case 12:
                AvatarOutAnimation();
                break;
            case 13:
                backPanel.SetActive(false);
                goPanel.SetActive(true);
                goAnim();


                break;
            case 14:
                buttonSet.SetActive(true);
                FlashCups();

                TimeController.instance.homeButton.SetActive(true);
                TimeController.instance.timerPannel.SetActive(true);
                TimeController.instance.ResumeTimer();
                
                OpenControlPanel();
                player.GetComponent<PoolPlayerController>().isStarted = true;
                ActiveInflatables();
                break;
            case 15:

                TimeController.instance.PauseTimer();
                StopInflatables();
                Stepper();

                break;
            case 16:

                bubbleStep++;
                textAreas[bubbleStep].text = "";
                AvatarInAnimation();
                BubbleInAnimation("SWIMMING_SB_5");
                break;
            case 17:

                StartCoroutine(WaitForNextStep(5f));
                break;
            case 18:

                buttonSet.SetActive(false);
                outroPanel.SetActive(true);
                TimeController.instance.homeButton.SetActive(false);
                TimeController.instance.timerPannel.SetActive(false);
                challengeCompletePanel.SetActive(true);
                player.SetActive(false);
                timeText.text = TimeController.instance.getFormattedGameTimeText(TimeController.instance.totalSeconds - startTime);
                break;
            case 19:

                challengeCompletePanel.SetActive(false);
                // nextChallengePanel.SetActive(true);
                // StartCoroutine(WaitForNextStep(3f));
                gameEndVideo.Play();
                break;
            case 20:

                gameEndVideo.Stop();
                SoundManager.instance.StopSound("SwimmerTheme");
                SoundManager.instance.PlaySound("NHSTheme");
                //nextChallengePanel.SetActive(false);
                //nhsimage.SetActive(true);
                NHSVideo.Play();
              //  StartCoroutine(WaitForNextStep(3f));

                break;
            case 21:

                SceneManager.LoadScene("NHS", LoadSceneMode.Single);
                break;
            default:
                break;
        }

    }

    private void BubbleInAnimation(string sound) {
        Debug.Log(bubbleStep);
        Vector3 bubblePosition = bubbles[bubbleStep].anchoredPosition;
        Debug.Log(bubblePosition);

        LeanTween.move(bubbles[bubbleStep], new Vector3(bubblePosition.x + 10f, bubblePosition.y, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(WriteBubbleText).setOnCompleteParam(sound);
        LeanTween.alpha(bubbles[bubbleStep], 1, 0.5f);
    }

    private void BubbleOutAnimation(int b) {
        Vector3 bubblePosition = bubbles[b].anchoredPosition;

        LeanTween.move(bubbles[b], new Vector3(bubblePosition.x, bubblePosition.y + 10f, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(Stepper);
        LeanTween.alpha(bubbles[b], 0, 0.5f);
    }

    private void AvatarInAnimation() {
        LeanTween.alpha(avatar, 1, 0.5f);
    }

    private void AvatarOutAnimation() {
        LeanTween.alpha(avatar, 0, 0.5f).setOnComplete(Stepper);
    }

    private void WriteBubbleText(object sound) {
        SoundManager.instance.PlaySound(sound.ToString());
        StartCoroutine(WriteText(
            textAreas[bubbleStep],
            texts[bubbleStep]
        ));
    }

    private void AddTextToBubble(string text) {
        StartCoroutine(AddText(
            textAreas[bubbleStep],
            text
        ));
    }

    private void OpenControlPanel() {
        TimeController.instance.ResumeTimer();
        buttonSet.SetActive(true);
    }

    IEnumerator WriteText(Text textArea, string text) {
        string currentText = "";
        for (int i = 0; i < text.Length + 1; i++) {
            currentText = text.Substring(0, i);
            textArea.text = currentText;
            yield return new WaitForSeconds(0.07f);
        }
        Stepper();
    }

    IEnumerator AddText(Text textArea, string text) {
        string oldText = textArea.text;
        string currentText = "";
        for (int i = 0; i < text.Length + 1; i++) {
            currentText = text.Substring(0, i);
            textArea.text = oldText + currentText;
            yield return new WaitForSeconds(0.07f);
        }
        Stepper();
    }


    IEnumerator WaitForNextStep(float m) {
        yield return new WaitForSeconds(m);
        Stepper();
    }



    #region Inflatable Collide

    public void ShowInflatableMessage(){
        hitBackPanel.SetActive(true);
        StopInflatables();
        //ShowInflatableAvatar();
        InflatableMessageBubbleInAnimation();
    }

    private void ShowInflatableAvatar() {
        LeanTween.alpha(avatar, 1, 0.5f).setOnComplete(InflatableMessageBubbleInAnimation);
    }

    private void InflatableMessageBubbleInAnimation() {

        Vector3 bubblePosition = bubbles[5].anchoredPosition;
        LeanTween.move(bubbles[5], new Vector3(bubblePosition.x - 10f, bubblePosition.y, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(InflatableMessageWriteBubbleText);
        LeanTween.alpha(bubbles[5], 1, 0.5f);
    }

    private void InflatableMessageWriteBubbleText() {
        SoundManager.instance.PlaySound("SWIMMING_SB_6");
        StartCoroutine(InflatableMessageWriteText(
          textAreas[5],
          texts[5]
      ));
    }
    private void InflatableMessageAvatarOutAnimation() {
        LeanTween.alpha(avatar, 0, 0.5f);
    }

    private void InflatableMessageBubbleOutAnimation() {
        Vector3 bubblePosition = bubbles[5].anchoredPosition;

        LeanTween.move(bubbles[5], new Vector3(bubblePosition.x, bubblePosition.y + 10f, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(StartGameAgain);
        LeanTween.alpha(bubbles[5], 0, 0.5f);
    }

    private void StartGameAgain() {
        hitBackPanel.SetActive(false);
        ActiveInflatables();
        player.GetComponent<PoolPlayerController>().ReStartGame();
    }

    private void ActiveInflatables() {
        for (int i = 0; i < inflatables.Length; i++) {
            inflatables[i].GetComponent<InflatableController>().ResumeAnimation();

        }
    }

    private void StopInflatables() {
        for (int i = 0; i < inflatables.Length; i++) {
            inflatables[i].GetComponent<InflatableController>().PauseAnimation();
        }
    }



    IEnumerator InflatableMessageWriteText(Text textArea, string text) {
        string currentText = "";
        for (int i = 0; i < text.Length + 1; i++) {
            currentText = text.Substring(0, i);
            textArea.text = currentText;
            yield return new WaitForSeconds(0.07f);
        }
        StartCoroutine(InflatableMessageWaitForNextStep(5));
    }

    IEnumerator InflatableMessageWaitForNextStep(float m) {
        yield return new WaitForSeconds(m);
      //  InflatableMessageAvatarOutAnimation();
        InflatableMessageBubbleOutAnimation();
    }

    #endregion

    private void VideoStepper(VideoPlayer vp) {
        Stepper();
    }

    public void skipVideo(){
        vp.Stop();
        Stepper();
    }

    private void FlashCups() {
        for (int i = 0; i < cups.Length; i++) {
            LeanTween.alpha(cups[i], 0, .5f).setLoopPingPong();

        }

    }

    public void CloseCups(string name) {
        for (int i = 0; i < cups.Length; i++) {
            cups[i].SetActive(false);
        }
        if (name == "GoldenCup") {
            cups[0].SetActive(true);
        } else if (name == "SilverCup") {
            cups[1].SetActive(true);
        } else if(name == "BronzeCup") {
            cups[2].SetActive(true);
        } 
    }

}
