﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class NHSSceneController : MonoBehaviour {

    public GameObject player;
    public GameObject buttonSetFor2;
    public GameObject buttonSetFor4;
    public RectTransform avatar;

    public GameObject backPanel;
    public GameObject outroPanel;
    public GameObject goPanel;
    public GameObject challengeCompletePanel;
    //public GameObject nextChallengePanel;
    //public GameObject spaceimage;
    public VideoPlayer swimVideo;
    public VideoPlayer gameEndVideo;
    public Text timeText;
    public Text goText1;
    public Text goText2;
    public GameObject twoPlayerPanel;

    public GameObject cardList;

    public RectTransform[] bubbles;
    public Text[] textAreas;
    public Button[] cards;
    public SpriteRenderer[] patientCards;
    public Sprite[] greyPatientCards;

    public int selectedCard;

    private int totalCard;
    private int step;
    private int bubbleStep;
    private string[] texts;
    private int goStep;
    private int startTime;

    // Use this for initialization
    void Start() {
        startTime = TimeController.instance.totalSeconds;
        step = -1;
        bubbleStep = 0;
        goStep = 4;

        step = -1;
        bubbleStep = 0;

        totalCard = 0;

        twoPlayerPanel.SetActive(false);
        goPanel.SetActive(false);
        cardList.SetActive(false);

        texts = new string[8];
        texts[0] = "Paramedic! You’re needed urgently!";
        texts[1] = "Technology helps doctors allocate organs to patients more quickly.";
        texts[2] = "You need to provide patients in a critical condition with a much-needed organ transplant – time is critical!";
        texts[3] = "Focus! You have 4 precious organs to deliver to 4 critically ill patients.";
        texts[4] = "Tap the screen to pick a donor card. Then drive your ambulance to the correct patient in the correct location.";
        texts[5] = "Excellent! Your quick thinking has helped save lives.";
        texts[6] = "You delivered the organ to the wrong patient, try again.";
        texts[7] = "Nice you've made the right match.";
        for (int i = 0; i < bubbles.Length; i++) {
            LeanTween.alpha(bubbles[i], 0, 0f);
        }
        LeanTween.alpha(avatar, 0, 0f);

        swimVideo.loopPointReached += VideoStepper;
        gameEndVideo.loopPointReached += VideoStepper;

        if (TimeController.instance.playerCount == 4) {
            PlayersAreReady();
        } else {
            twoPlayerPanel.SetActive(true);
        }

    }

    // Update is called once per frame
    void Update() {

    }

    public void PlayersAreReady() {
        twoPlayerPanel.SetActive(false);
        backPanel.SetActive(true);
        AvatarInAnimation();
        Stepper();
    }

    private void goAnim() {
        goStep--;
        if (goStep > 0) {
            goText1.text = goStep.ToString();
            goText2.text = goStep.ToString();
            LeanTween.alpha(goPanel, 1, 1).setOnComplete(goAnim);
        } else if (goStep == 0) {
            goText1.text = "GO!";
            goText2.text = "GO!";
            LeanTween.alpha(goPanel, 1, 1).setOnComplete(goAnim);
        } else {
            goPanel.SetActive(false);
            // AvatarInAnimation();
             Stepper();
        }

    }

    /**** Safari game steps
            0 - First bubble and texts
            1 - Wait for reading
            2 - 
            3 - Close First bubble
            4 - Second bubble and texts
            5 - Wait for reading
            6 - Close second bubble
            7 - Third bubble and texts
            8 - Wait for reading
            9 - Close Third bubble / Avatar out animation
            10 - Open Control panel
            11 - Close Control panel / Open avatar / Open Fourth (End game) bubble
            12 - Wait for reading
            13 - Close Fourth Bubble / Avatar out animation
            14 - 
            15 - 
            16 - 


        ***/
    public void Stepper() {
        step++;
        Debug.Log("step " + step);

        switch (step) {
            case 0:
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("NHS_SB_1");
                break;
            case 1:
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 2:
                BubbleOutAnimation(0);
                break;
            case 3:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("NHS_SB_2");
                break;
            case 4:
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 5:
                BubbleOutAnimation(1);
                break;
            case 6:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("NHS_SB_3");
                break;
            case 7:
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 8:
                BubbleOutAnimation(2);
                break;
            case 9:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("NHS_SB_4");
                break;
            case 10:
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 11:
                BubbleOutAnimation(3);
                break;
            case 12:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("NHS_SB_5");
                break;
            case 13:
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 14:
                BubbleOutAnimation(4);
                AvatarOutAnimation();
                break;
            case 15:
                backPanel.SetActive(false);
                goPanel.SetActive(true);
                goAnim();
                break;
            case 16:
                TimeController.instance.homeButton.SetActive(true);
                TimeController.instance.timerPannel.SetActive(true);
                TimeController.instance.ResumeTimer();
                OpenCardList();
                player.GetComponent<NHSPlayerController>().GetPlayerToScene();
                break;
            case 17:
                TimeController.instance.PauseTimer();
                backPanel.SetActive(true);
                AvatarInAnimation();
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("NHS_SB_7");
                break;
            case 18:
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 19:
                CloseControlPanel();
                outroPanel.SetActive(true);
                TimeController.instance.homeButton.SetActive(false);
                TimeController.instance.timerPannel.SetActive(false);
                challengeCompletePanel.SetActive(true);
                player.SetActive(false);
                timeText.text = TimeController.instance.getFormattedGameTimeText(TimeController.instance.totalSeconds - startTime);
                break;
            case 20:
                gameEndVideo.Play();
                break;
            case 21:
                gameEndVideo.Stop();
                SoundManager.instance.StopSound("NHSTheme");
                SoundManager.instance.PlaySound("SwimmerTheme");
               // nextChallengePanel.SetActive(false);
                swimVideo.Play();
                //spaceimage.SetActive(true);
                //StartCoroutine(WaitForNextStep(3f));
                break;
            case 22:
                SceneManager.LoadScene("Pool", LoadSceneMode.Single);
                break;
            default:
                break;
        }

    }

    public void SelectCard(int card) {
        player.GetComponent<NHSPlayerController>().OpenCard(card);
        cards[card].interactable = false;
        selectedCard = card;
        CloseCardList();
        OpenControlPanel();
    }

    public void RightPatient(int card) {
        CloseControlPanel();
        ChangePatientCardColor(card);
        totalCard++;
        if(totalCard == 4) {
            Stepper();
        } else {
            CorrectPatientMessageBubbleInAnimation();

        }
    }

    public void WrongPatient() {
        CloseControlPanel();
        WrongPatientMessageBubbleInAnimation();
    }

    private void ChangePatientCardColor(int card) {
        patientCards[card].sprite = greyPatientCards[card];
    }



    private void AvatarInAnimation() {
        LeanTween.alpha(avatar, 1, 0.5f);
    }

    private void AvatarOutAnimation() {
        LeanTween.alpha(avatar, 0, 0.5f);
    }

    private void BubbleInAnimation(string sound) {
        Debug.Log(bubbleStep);
        Vector3 bubblePosition = bubbles[bubbleStep].anchoredPosition;
        Debug.Log(bubblePosition);
       
        LeanTween.move(bubbles[bubbleStep], new Vector3(bubblePosition.x, bubblePosition.y - 10f, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(WriteBubbleText).setOnCompleteParam(sound);
        LeanTween.alpha(bubbles[bubbleStep], 1, 0.5f);
    }

    private void BubbleOutAnimation(int b) {
        Vector3 bubblePosition = bubbles[b].anchoredPosition;

        LeanTween.move(bubbles[b], new Vector3(bubblePosition.x, bubblePosition.y + 10f, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(Stepper);
        LeanTween.alpha(bubbles[b], 0, 0.5f);
    }

    private void WriteBubbleText(object sound) {
        SoundManager.instance.PlaySound(sound.ToString());
        StartCoroutine(WriteText(
            textAreas[bubbleStep],
            texts[bubbleStep]
        ));
    }

    private void OpenControlPanel() {
        //  TimeController.instance.StartTimer(120);
        if (TimeController.instance.playerCount == 4) {
            buttonSetFor4.SetActive(true);
        } else {
            buttonSetFor2.SetActive(true);
        }
    }

    private void CloseControlPanel() {
        buttonSetFor4.SetActive(false);
    }

    private void OpenCardList() {
        cardList.SetActive(true);
    }

    private void CloseCardList() {
        cardList.SetActive(false);
    }

    IEnumerator WriteText(Text textArea, string text) {
        string currentText = "";
        for (int i = 0; i < text.Length + 1; i++) {
            currentText = text.Substring(0, i);
            textArea.text = currentText;
            yield return new WaitForSeconds(0.07f);
        }
        Stepper();
    }

    IEnumerator WaitForNextStep(float m) {
        yield return new WaitForSeconds(m);
        Stepper();
    }

    private void VideoStepper(VideoPlayer vp) {
        Stepper();
    }

    #region wrong_patient
    private void WrongPatientMessageBubbleInAnimation() {
        textAreas[6].text = "";
        Vector3 bubblePosition = bubbles[6].anchoredPosition;
        LeanTween.move(bubbles[6], new Vector3(bubblePosition.x - 10f, bubblePosition.y, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(WrongPatientMessageWriteBubbleText);
        LeanTween.alpha(bubbles[6], 1, 0.5f);
    }

    private void WrongPatientMessageWriteBubbleText() {
        SoundManager.instance.PlaySound("NHS_SB_6");
        StartCoroutine(WrongPatientMessageWriteText(
          textAreas[6],
          texts[6]
      ));
    }

    private void WrongPatientMessageBubbleOutAnimation() {
        Vector3 bubblePosition = bubbles[6].anchoredPosition;

        LeanTween.move(bubbles[6], new Vector3(bubblePosition.x, bubblePosition.y + 10f, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(StartGameAgain);
        LeanTween.alpha(bubbles[6], 0, 0.5f);
    }

    private void StartGameAgain() {
        OpenControlPanel();
    }

    IEnumerator WrongPatientMessageWriteText(Text textArea, string text) {
        string currentText = "";
        for (int i = 0; i < text.Length + 1; i++) {
            currentText = text.Substring(0, i);
            textArea.text = currentText;
            yield return new WaitForSeconds(0.07f);
        }
        StartCoroutine(WrongPatientMessageWaitForNextStep(5));
    }

    IEnumerator WrongPatientMessageWaitForNextStep(float m) {
        yield return new WaitForSeconds(m);
        //  InflatableMessageAvatarOutAnimation();
        WrongPatientMessageBubbleOutAnimation();
    }

    #endregion

    #region correct_patient
    private void CorrectPatientMessageBubbleInAnimation() {
        textAreas[6].text = "";
        Vector3 bubblePosition = bubbles[6].anchoredPosition;
        LeanTween.move(bubbles[6], new Vector3(bubblePosition.x - 10f, bubblePosition.y, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(CorrectPatientMessageWriteBubbleText);
        LeanTween.alpha(bubbles[6], 1, 0.5f);
    }

    private void CorrectPatientMessageWriteBubbleText() {

        StartCoroutine(CorrectPatientMessageWriteText(
          textAreas[6],
          texts[7]
      ));
    }

    private void CorrectPatientMessageBubbleOutAnimation() {
        Vector3 bubblePosition = bubbles[6].anchoredPosition;

        LeanTween.move(bubbles[6], new Vector3(bubblePosition.x, bubblePosition.y + 10f, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(OpenCardList);
        LeanTween.alpha(bubbles[6], 0, 0.5f);
    }

 
    IEnumerator CorrectPatientMessageWriteText(Text textArea, string text) {
        string currentText = "";
        for (int i = 0; i < text.Length + 1; i++) {
            currentText = text.Substring(0, i);
            textArea.text = currentText;
            yield return new WaitForSeconds(0.07f);
        }
        StartCoroutine(CorrectPatientMessageWaitForNextStep(3));
    }

    IEnumerator CorrectPatientMessageWaitForNextStep(float m) {
        yield return new WaitForSeconds(m);
        //  InflatableMessageAvatarOutAnimation();
        CorrectPatientMessageBubbleOutAnimation();
    }

    #endregion

}
