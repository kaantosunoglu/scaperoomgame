﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {


    public static SoundManager instance;

    [SerializeField]
    public Sound[] sounds;

    void Awake() {
        if (instance == null) {
            instance = this;
            return;
        }
    }

    // Use this for initialization
    void Start () {
        DontDestroyOnLoad(this);
        for (int i = 0; i < sounds.Length; i++) {
            GameObject soundGameObject = new GameObject("Sound_" + i + "_" + sounds[i].name);
            sounds[i].SetAudioSource(soundGameObject.AddComponent<AudioSource>());
            DontDestroyOnLoad(soundGameObject);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StopAllSounds() {
        for (int i = 0; i < sounds.Length; i++) {

            sounds[i].Stop();

        }
    }

    public void PlaySound(string name) {
        for (int i = 0; i < sounds.Length; i++){
            if(name == sounds[i].name) {
                sounds[i].Play();
            }
        }
    }

    public void StopSound(string name) {
        for (int i = 0; i < sounds.Length; i++) {
            if (name == sounds[i].name) {
                sounds[i].Stop();
            }
        }
    }

    public void PauseSound(string name) {
        for (int i = 0; i < sounds.Length; i++) {
            Debug.Log(name +" == " +sounds[i].name);
            if (name == sounds[i].name) {
                Debug.Log(sounds[i].name);
                sounds[i].Pause();
            }
        }
    }

    public void ResumeSound(string name) {
        for (int i = 0; i < sounds.Length; i++) {
            if (name == sounds[i].name) {
                sounds[i].UnPause();
            }
        }
    }
}
