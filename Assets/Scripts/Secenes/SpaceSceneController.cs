﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class SpaceSceneController : MonoBehaviour {

    public GameObject player;
    public GameObject earth;
    public GameObject buttonSetFor2;
    public GameObject buttonSetFor4;
    public RectTransform avatar;
    public GameObject outroPanel;
    public GameObject challengeCompletePanel;
    public Text timeText;

    public RectTransform[] bubbles;
    public Text[] textAreas;

    public GameObject[] blackHoles;

    public GameObject backPanel;
    public GameObject goPanel;
    public Text goText1;
    public Text goText2;
    public GameObject twoPlayerPanel;

    private string[] texts;
    private int step;
    private int bubbleStep;
    private int codeStep;
    private int startTime;
    private int goStep;

    private bool isStarTaken;

    // Use this for initialization
    void Start() {
        startTime = TimeController.instance.totalSeconds;
        step = -1;
        bubbleStep = 0;
        isStarTaken = false;
        //backPanel.SetActive(true);
        goPanel.SetActive(false);
        twoPlayerPanel.SetActive(false);
        texts = new string[7];
        texts[0] = "Hi! I’m Mazie. I’m here to help you with this task.\nYou’ve got to make your way through this asteroid field, only we’re running out of time!";
        texts[1] = "I’ve learnt from everyone that’s gone before you, so with my advice you can’t fail.";
        texts[2] = "Quick! Return to the middle of the screen! \nYou’ve got to avoid the black holes.";
        texts[3] = "Remember, the stars will help you, grab them where you can!";
        texts[4] = "Excellent, we made it! ";
        texts[5] = "I hope to see you again soon!";
        texts[6] = "Oh no!\nLooks like you're not listening\n\nI'm here to help \nLet's get you back on track.";
        for (int i = 0; i < bubbles.Length; i++) {
            LeanTween.alpha(bubbles[i], 0, 0f);
        }
        LeanTween.alpha(avatar, 0, 0f);

        goStep = 4;

        if (TimeController.instance.playerCount == 4) {
            PlayersAreReady();
        } else {
            twoPlayerPanel.SetActive(true);
        }



        //        player.GetComponent<PlayerController>().GetPlayerToScene();
    }

    // Update is called once per frame
    void Update() {

    }

    public void PlayersAreReady() {
        twoPlayerPanel.SetActive(false);
        backPanel.SetActive(true);
        AvatarInAnimation();
        Stepper();
    }

    private void goAnim() {
        goStep--;
        if (goStep > 0) {
            goText1.text = goStep.ToString();
            goText2.text = goStep.ToString();
            LeanTween.alpha(goPanel, 1, 1).setOnComplete(goAnim);
        } else if (goStep == 0) {
            goText1.text = "GO!";
            goText2.text = "GO!";
            LeanTween.alpha(goPanel, 1, 1).setOnComplete(goAnim);
        } else {
            goPanel.SetActive(false);
            // AvatarInAnimation();
             Stepper();
        }

    }

    /**** Safari game steps
      0 - Space Sound play 
      1 - First bubble and texts
      2 - Wait for reading
      3 - Close First bubble
      4 - Second bubble and texts
      5 - Wait for reading
      6 - Close second bubble
      7 - Third bubble and texts
      8 - Wait for reading
      9 - Close Third bubble / Avatar out animation
      10 - Open Control panel
      11 - Close Control panel / Open avatar / Open Fourth (End game) bubble
      12 - Wait for reading
      13 - Close Fourth Bubble / Avatar out animation
      14 - 
      15 - 
      16 - 


  ***/
    public void Stepper() {
        step++;
        Debug.Log("step " + step);

        switch (step) {
            case 0:
                SoundManager.instance.PlaySound("SpaceAmbience");
                StartCoroutine(WaitForNextStep(1.5f));
                break;
            case 1:
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("SPACE_SB_1");
                break;
            case 2:
                StartCoroutine(WaitForNextStep(4f));
                break;
            case 3:
                BubbleOutAnimation(0);
                break;
            case 4:
                StartCoroutine(WaitForNextStep(1f));
                break;
            case 5:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("SPACE_SB_2");
                break;
            case 6:

                BubbleOutAnimation(1);
                break;
            case 7:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("SPACE_SB_3");

                break;
            case 8:
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 9:
                BubbleOutAnimation(2);
                break;
           case 10:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("SPACE_SB_4");
                break;
            case 11:
                StartCoroutine(WaitForNextStep(5f));
                break;

            case 12:

                AvatarOutAnimation();
                BubbleOutAnimation(2);
                break;
            case 13:
                backPanel.SetActive(false);
                goPanel.SetActive(true);
                goAnim();
               
                break;
            case 14:
                OpenControlPanel();
                InvokeRepeating("SpaceFlashing", 0, 1);
                TimeController.instance.homeButton.SetActive(true);
                TimeController.instance.timerPannel.SetActive(true);
                TimeController.instance.ResumeTimer();
                break;
            case 15:
                TimeController.instance.PauseTimer();
                SoundManager.instance.PlaySound("MissionComplete");
                CloseControlPanel();
                AvatarInAnimation();
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("SPACE_SB_5");
                break;
            case 16:
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 17:
                BubbleOutAnimation(3);
                break;
            case 18:
                bubbleStep++;
                textAreas[bubbleStep].text = "";
                BubbleInAnimation("SPACE_SB_6");
                break;
            case 19:
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 20:
                AvatarOutAnimation();
                BubbleOutAnimation(4);
                break;
            case 21:
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 22:
                outroPanel.SetActive(true);
                //TimeController.instance.homeButton.SetActive(false);
                TimeController.instance.timerPannel.SetActive(false);
                challengeCompletePanel.SetActive(true);
                player.SetActive(false);
                timeText.text = TimeController.instance.getFormattedGameTimeText(TimeController.instance.totalSeconds);
                StartCoroutine(WaitForNextStep(5f));
                break;
            case 23:
                SceneManager.LoadScene("Outro", LoadSceneMode.Single);
                break;

            default:
                break;
        }

    }

    public void StopSpaceFlashing()
    {
        CancelInvoke("SpaceFlashing");
    }

    void SpaceFlashing()
    {
        if (player.activeSelf)
        {
            player.SetActive(false);
            earth.SetActive(false);
        }
        else
        {
            player.SetActive(true);
            earth.SetActive(true);
        }
    }

    private void BubbleInAnimation(string sound) {
        Debug.Log(bubbleStep);
        Vector3 bubblePosition = bubbles[bubbleStep].anchoredPosition;
        Debug.Log(bubblePosition);

        LeanTween.move(bubbles[bubbleStep], new Vector3(bubblePosition.x, bubblePosition.y + 10f, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(WriteBubbleText).setOnCompleteParam(sound);
        LeanTween.alpha(bubbles[bubbleStep], 1, 0.5f);
    }

    private void BubbleOutAnimation(int b) {
        Vector3 bubblePosition = bubbles[b].anchoredPosition;

        LeanTween.move(bubbles[b], new Vector3(bubblePosition.x, bubblePosition.y + 10f, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(Stepper);
        LeanTween.alpha(bubbles[b], 0, 0.5f);
    }

    private void WriteBubbleText(object sound) {
        SoundManager.instance.PlaySound(sound.ToString());
        StartCoroutine(WriteText(
            textAreas[bubbleStep],
            texts[bubbleStep]
        ));
    }

    private void AvatarInAnimation() {
        LeanTween.alpha(avatar, 1, 0.5f);
    }

    private void AvatarOutAnimation() {
        LeanTween.alpha(avatar, 0, 0.5f);
    }

    private void OpenControlPanel() {
        TimeController.instance.ResumeTimer();
        if(TimeController.instance.playerCount == 2)
        {
            buttonSetFor2.SetActive(true);
        }
        else
        {
            buttonSetFor4.SetActive(true);
        }
    }

    private void CloseControlPanel() {
        buttonSetFor4.SetActive(false);
        buttonSetFor2.SetActive(false);
    }

    IEnumerator WriteText(Text textArea, string text) {
        string currentText = "";
        for (int i = 0; i < text.Length + 1; i++) {
            currentText = text.Substring(0, i);
            textArea.text = currentText;
            yield return new WaitForSeconds(0.07f);
        }
        Stepper();
    }

    IEnumerator WaitForNextStep(float m) {
        yield return new WaitForSeconds(m);
        Stepper();
    }


    public void TurnOffHoles() {
        if (!isStarTaken) {
            for (int i = 0; i < blackHoles.Length; i++) {
                LeanTween.alpha(blackHoles[i], 0f, .1f);
                blackHoles[i].GetComponent<BoxCollider2D>().enabled = false;
            }
            isStarTaken = true;
            TurnOnHoles();

        }

    }

    private void TurnOnHoles() {
        Debug.Log("+++++   " + isStarTaken);
        if(isStarTaken) {
            for (int i = 0; i < blackHoles.Length; i++) {
                LeanTween.alpha(blackHoles[i], 1f, .1f).setDelay(10f);
            }
            LeanTween.delayedCall(gameObject, 11f, TurnOnHolesCollider);
        }
        //isStarTaken = false;
    }



    private void TurnOnHolesCollider() {
        Debug.Log("SSS 2");
        for (int i = 0; i < blackHoles.Length; i++) {
            blackHoles[i].GetComponent<BoxCollider2D>().enabled = true;
        }
        isStarTaken = false;
    }

    #region hitwall
    public void HitWallErrorMessage() {
        CloseControlPanel();
        AvatarInAnimation();
        HitWallBubbleInAnimation();
    }
    private void HitWallBubbleInAnimation() {
        SoundManager.instance.PlaySound("SWIMMING_SB_7");
        Vector3 bubblePosition = bubbles[7].anchoredPosition;
        Debug.Log(bubblePosition);

        LeanTween.move(bubbles[7], new Vector3(bubblePosition.x, bubblePosition.y + 10f, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(HitWallBubbleWriteBubbleText);
        LeanTween.alpha(bubbles[7], 1, 0.5f);
    }

    private void HitWallBubbleOutAnimation() {
        SoundManager.instance.PlaySound("SWIMMING_SB_7");

        Vector3 bubblePosition = bubbles[7].anchoredPosition;

        LeanTween.move(bubbles[7], new Vector3(bubblePosition.x, bubblePosition.y + 10f, bubblePosition.z), 0.6f).setEase(LeanTweenType.easeInQuad).setOnComplete(ReStart);
        LeanTween.alpha(bubbles[7], 0, 0.5f);
    }

    private void HitWallBubbleWriteBubbleText() {
        textAreas[7].text = "";
        StartCoroutine(HitWallBubbleWriteText(
            textAreas[7],
            texts[6]
        ));

    }

    private void ReStart() {
        player.transform.position = new Vector3(-8.463f, 3.53f, 0f);
        OpenControlPanel();
    }

    IEnumerator HitWallBubbleWriteText(Text textArea, string text) {
        string currentText = "";
        for (int i = 0; i < text.Length + 1; i++) {
            currentText = text.Substring(0, i);
            textArea.text = currentText;
            yield return new WaitForSeconds(0.07f);
        }
        Debug.Log("TEST 1");
        StartCoroutine(HitWallBubbleWaitForNextStep(5f));
    }

    IEnumerator HitWallBubbleWaitForNextStep(float m) {
        Debug.Log("TEST 2");
        yield return new WaitForSeconds(m);
        Debug.Log("TEST 3");
        AvatarOutAnimation();
        HitWallBubbleOutAnimation();

    }

    #endregion


}
