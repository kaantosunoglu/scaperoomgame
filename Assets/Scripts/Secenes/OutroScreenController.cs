﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class OutroScreenController : MonoBehaviour {

    public Text time;
    public GameObject timeText;
    public VideoPlayer vp1;
    public VideoPlayer vpLooping;
    public GameObject homeButton;


    // Use this for initialization
    void Start () {
        TimeController.instance.homeButton.SetActive(false);
        TimeController.instance.gameFinished = true;
        TimeSpan timeSpan = TimeSpan.FromSeconds(TimeController.instance.totalSeconds);
        time.text = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
        vp1.loopPointReached += playLoopingOutro;
	}

    private void playLoopingOutro(VideoPlayer source)
    {
        timeText.transform.localPosition = new Vector3(883.0f, -450f, 0);
        vpLooping.Play();
    }

    // Update is called once per frame
    void Update () {
        if (vp1.frame == 544)
        {
            homeButton.SetActive(true);
        }
    }

    public void ExitToMainScene()
    {
        SoundManager.instance.StopAllSounds();
        TimeController.instance.ExitToMainScene();
    }
}
