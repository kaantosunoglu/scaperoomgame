﻿using UnityEngine;
using Leguar.SegmentDisplay;
using UnityEngine.SceneManagement;
using System;
using System.IO;

public class TimeController : MonoBehaviour {

    public static TimeController instance;

    public SegmentDisplay display;

    public GameObject timerPannel;
    public GameObject homeButton;

    public GameObject ExitToHomePanel;

    public int playerCount;

    private RectTransform timerPannelRT;
    private RectTransform homeButtonRT;

    // SDController from Segment Display
    private SDController sdController;

    // Total Seconds for play
    private int targetSeconds;

    // Total Seconds from beginning
    public int totalSeconds;
    private float analyticsSeconds;
    public bool gameFinished;
    private string analyticsPath = "analytics.csv";

    // Inactivity counter
    private float inactivityCounter = 300.0f;
    private float inactivityTimer = 0f;

    void Awake() {
        if (instance == null) {
            instance = this;
            return;
        }
    }

    // Use this for initialization
    void Start()
    {
        // Dont destroy on load
        // to use every game scene
        DontDestroyOnLoad(this.gameObject);
        sdController = display.GetSDController();
        totalSeconds = 0;
        sdController.AddCommand(new SetTextCommand(getFormattedTimerText(totalSeconds)));
        SceneManager.activeSceneChanged += ChangeCanvasSetup;
        timerPannelRT = timerPannel.GetComponent<RectTransform>();
        homeButtonRT = homeButton.GetComponent<RectTransform>();
        timerPannel.SetActive(false);
        homeButton.SetActive(true);
        if (!File.Exists(analyticsPath))
        {
            using (StreamWriter sw = File.CreateText(analyticsPath))
            {
                sw.WriteLine(String.Format("{0},{1},{2},{3}", "\"Time spent in the game in seconds\"", "\"No of players\"", "\"Game Finished\"", "\"Home button / Inactivity / Exit\""));
            }
        }
    }

    public void StartAnalytics()
    {
        instance.analyticsSeconds = 0;
        gameFinished = false;
    }

    void SaveAnalytics(string homeOrInactivity = "")
    {
        using (StreamWriter sw = File.AppendText(analyticsPath))
        {
            sw.WriteLine(String.Format("{0},{1},{2},{3}", (int)instance.analyticsSeconds, instance.playerCount, gameFinished.ToString(), homeOrInactivity));
        }
    }

    void OnApplicationQuit()
    {
        SaveAnalytics("Game Closed");
    }

    // Update is called once per frame
    void Update () {
        instance.analyticsSeconds += Time.deltaTime;
        inactivityTimer += Time.deltaTime;
        if (Input.GetMouseButtonDown(0))
        {
            inactivityTimer = 0f;
        }
        if(inactivityTimer > inactivityCounter)
        {
            inactivityTimer = 0f;
            SoundManager.instance.StopAllSounds();
            ExitToMainScene("Inactivity");
        }
	}

    public void StartTimer(int targetSeconds) {
        totalSeconds = 0;
        sdController.AddCommand(new SetTextCommand(getFormattedTimerText(totalSeconds)));
        this.targetSeconds = targetSeconds;
        StartTimer();
    }

    private void StartTimer() {

        int ts = totalSeconds;
        for (int s = ts; s <= targetSeconds; s++) {
            sdController.AddCommand(new SetTextCommand(getFormattedTimerText(s)));
            sdController.AddCommand(new PauseCommand(1f));
            sdController.AddCommand(new CallbackCommand(() => totalSeconds++));
         }

        // Call another method in this class after timer is done
        sdController.AddCommand(new CallbackCommand(timerFinished));

        // Blink "End" for a while
        for (int n = 0; n < 5; n++) {
            sdController.AddCommand(new SetTextCommand("End", SegmentDisplay.Alignments.Left));
            sdController.AddCommand(new PauseCommand(0.5f));
            sdController.AddCommand(new ClearCommand());
            sdController.AddCommand(new PauseCommand(0.5f));
        }
    }

    public void PauseTimer() {
        sdController.ClearCommandsAndStop();
    }

    public void ResumeTimer() {
        StartTimer();
    }

    public void StopTimer() {
        PauseTimer();
        totalSeconds = 0;
        sdController.AddCommand(new SetTextCommand(getFormattedTimerText(totalSeconds)));
    }

    public void KnockSeconds(int second) {
        PauseTimer();
        totalSeconds -= second;
        StartTimer();
    }

    void timerFinished() {
        // Ding! (At this point "End" is still blinking on display)
    }

    private string getFormattedTimerText(int ts) {
        if (ts < 10) {
            return ("00:0" + ts);
        }
        if (ts < 60) {
            return ("00:" + ts);
        }
        int minutes = ts / 60;
        int seconds = ts % 60;
        return ((minutes < 10 ? "0" : "" ) + minutes + ":" + (seconds < 10 ? "0" : "") + seconds);
    }

    public string getFormattedGameTimeText(int ts) {
        if (ts < 10) {
            return ("TIME 00’0" + ts);
        }
        if (ts < 60) {
            return ("TIME 00’" + ts);
        }
        int minutes = ts / 60;
        int seconds = ts % 60;
        return ("TIME " + (minutes < 10 ? "0" : "") + minutes + "’" + (seconds < 10 ? "0" : "") + seconds);
    }

    public void ReturnToMainScene() {
        ExitToHomePanel.SetActive(true);
        PauseTimer();
        //SoundManager.instance.StopAllSounds();
        //SceneManager.LoadScene("Main", LoadSceneMode.Single);
    }

    public void ResumeGame() {
        ResumeTimer();
        ExitToHomePanel.SetActive(false);
    }

    //Exit
    public void ExitToMainScene(string homeOrInactivity = "") {
        ResumeTimer();
        StopTimer();
        SaveAnalytics(homeOrInactivity);
        ExitToHomePanel.SetActive(false);
        SceneManager.LoadScene("Main", LoadSceneMode.Single);
    }

    public void OpenTimer() {

    }

    private void ChangeCanvasSetup(Scene current, Scene next) {
       // timerPannel.SetActive(true);
       // homeButton.SetActive(true);
        switch (next.name) {
            case "Main":
                timerPannel.SetActive(false);
                timerPannelRT.rotation = Quaternion.Euler(0f, 0f, 0f);
                timerPannelRT.anchoredPosition = new Vector2(50f, -50f);
                homeButtonRT.rotation = Quaternion.Euler(0f, 0f, 0f);
                homeButtonRT.anchoredPosition = new Vector2(-60f, -50f);
                break;
            case "Safari":
                timerPannelRT.rotation = Quaternion.Euler(0f, 0f, 0f);
                timerPannelRT.anchoredPosition = new Vector2(50f, -50f);
                homeButtonRT.rotation = Quaternion.Euler(0f, 0f, 0f);
                homeButtonRT.anchoredPosition = new Vector2(-60f, -50f);
                break;
            case "Pool":
                timerPannelRT.rotation = Quaternion.Euler(0f, 0f, 90f);
                timerPannelRT.anchoredPosition = new Vector2(105f, -1000f);
                homeButtonRT.rotation = Quaternion.Euler(0f, 0f, 90f);
                homeButtonRT.anchoredPosition = new Vector2(-1810f, -88f);
                break;
            case "Space":
                timerPannelRT.rotation = Quaternion.Euler(0f, 0f, -90f);
                timerPannelRT.anchoredPosition = new Vector2(1810f, -88f);
                homeButtonRT.rotation = Quaternion.Euler(0f, 0f, -90f);
                homeButtonRT.anchoredPosition = new Vector2(-105f, -1000);
                break;
            case "NHS":
                timerPannelRT.rotation = Quaternion.Euler(0f, 0f, 180f);
                timerPannelRT.anchoredPosition = new Vector2(1870f, -1030f);
                homeButtonRT.rotation = Quaternion.Euler(0f, 0f, 180f);
                homeButtonRT.anchoredPosition = new Vector2(-1870f, -1030f);
                break;
            default:
                break;
        }
    }
}