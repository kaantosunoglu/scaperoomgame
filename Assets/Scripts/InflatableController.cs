﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InflatableController : MonoBehaviour {


    public enum Direction {
        Right,
        Left

    }

    public float time;
    public Direction startDirection = Direction.Right;
    public float distance;


    private Vector3 startPosition;
    private float direction;

    private Vector3[] points;
    private Vector3[] controls;
    private int point = -1;
    private int nextPoint = -1;
    private int leanID;

    // Use this for initialization
    void Start() {
        points = new Vector3[6];
        controls = new Vector3[6];
        startPosition = transform.position;
        points[0] = startPosition;
        direction = startDirection == Direction.Right ? 1 : -1;
        FindRandomPoints();
    }


    private void FindRandomPoints() {
        //first point = start point 
        controls[0] = new Vector3(Random.Range(startPosition.x - distance, startPosition.x + distance),
                                          Random.Range(startPosition.y - distance, startPosition.y + distance),
                                           0f);

        for (int i = 1; i < points.Length; i++) {
            float xc = (i < 4 ? 1 : -1) * distance;
            float yc = (i < 3 ? 1 : -1) * distance;
            points[i] = new Vector3(Random.Range(points[i - 1].x + (xc / 2), points[i - 1].x + xc),
                                          Random.Range(points[i - 1].y - yc, points[i - 1].y - (yc / 2)),
                                           0f);
        }

        for (int i = 1; i < controls.Length; i++) {
            controls[i] = new Vector3(Random.Range(points[i].x - distance, points[i].x + distance),
                                          Random.Range(points[i].y - distance, points[i].y + distance),
                                           0f);
        }
        Animate();
        LeanTween.pause(leanID);
    }

    private void Animate() {
        if (startDirection == Direction.Right) {
            point = point == points.Length - 1 ? 0 : point + 1;
            nextPoint = point == points.Length - 1 ? 0 : point + 1;
        } else {
            point = point <= 0 ? points.Length - 1 : point - 1;
            nextPoint = point == 0 ? controls.Length - 1 : point - 1;
        }

        LTBezierPath ltPath = new LTBezierPath(new Vector3[] { new Vector3(points[point].x, points[point].y, 0f), new Vector3(controls[nextPoint].x, controls[nextPoint].y, 0f), new Vector3(controls[point].x, controls[point].y, 0f), new Vector3(points[nextPoint].x, points[nextPoint].y, 0f) });
        leanID = LeanTween.move(gameObject, ltPath, time / 6).setOrientToPath(false).setEase(LeanTweenType.easeInOutQuad).setOnComplete(Animate).id; // animate 
    }


    public void PauseAnimation(){
        LeanTween.pause(leanID);
    }

    public void ResumeAnimation() {
        LeanTween.resume(leanID);
    }

    // Update is called once per frame
    void Update() {

    }
}
