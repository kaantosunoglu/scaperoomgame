﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalTagControl : MonoBehaviour {

    public GameObject tagName;

    private bool isTagged;
    private float targetY;
    public Sprite glow;
    public GameObject glowFX;
    public SafariSceneController sceneController;
    public bool isVisible = false;

	// Use this for initialization
	void Start () {
        isTagged = false;
       // LeanTween.alpha(tagName, 0f, 0f);
        targetY = tagName.transform.localPosition.y + .1f;
        Debug.Log(tagName.transform.localPosition.y + "    " + targetY);
        //if (isVisible) {
        //    LeanTween.alpha(gameObject, 0f, 0f);
        //}
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Tagged() {
        if (!isTagged) {
            SoundManager.instance.PlaySound("correct");
            LeanTween.alpha(tagName, 1f, 1f);
            LeanTween.moveLocalY(tagName, targetY, 1f);
            gameObject.GetComponent<SpriteRenderer>().sprite = glow;
            sceneController.AnimalTagged();
            isTagged = true;
            LeanTween.alpha(gameObject, 0f, 1f).setDelay(3);
            LeanTween.alpha(tagName, 0f, 1f).setDelay(3);
        }

    }

    public void Shining() {
        LeanTween.alpha(gameObject, 1f, 1f).setOnComplete(shiningLoop);
        glowFX.SetActive(true);
        LeanTween.alpha(tagName, 1f, .7f).setDelay(1);
        LeanTween.moveLocalY(tagName, targetY, .7f).setDelay(1);
       

    }

    private void shiningLoop() {
        LeanTween.alpha(glowFX, 0f, .3f).setLoopPingPong();
    }


}
