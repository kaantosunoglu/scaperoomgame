﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;


public class AnalyticsManager : MonoBehaviour {

    public static AnalyticsManager instance;

    private int quitGameCounter;

    void Awake() {
        if (instance == null) {
            instance = this;
            return;
        }
    }

    // Use this for initialization
    void Start() {
        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update() {

    }

    public void GameStart(int players) {
        //AnalyticsEvent.GameStart(new Dictionary<string, object>
        //{
        //    { "players", players }
        //});
        AnalyticsEvent.Custom("Game_Start", new Dictionary<string, object>
        {
            { "Players", players }
        });

    }

    public void LevelStart(string level) {
        AnalyticsEvent.LevelStart(level);
    }

    public void LevelFinish(string level, float time) {
        AnalyticsEvent.LevelComplete(level, new Dictionary<string, object>{
            { "time", time}
        });
    }

    public void GameEnd(int time) {
        AnalyticsEvent.Custom("Game_End", new Dictionary<string, object>
            {
                { "Total_Time", time }
            }
        );
    }

    public void GameCancelled(string level) {
        AnalyticsEvent.Custom("Game_Cancelled", new Dictionary<string, object>
            {
                { "Maze", level }
            }
        );
    }

    public void quitGame(Button b){
        b.interactable = false;
        quitGameCounter++;
        if(quitGameCounter == 3){
            Debug.Log("sssss");
            Application.Quit();
        }

    }


}
