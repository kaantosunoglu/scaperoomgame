﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class SafariPlayerController : MonoBehaviour {


    public GameObject player;
    public Rigidbody2D playerRB;
    public float speed;

    public Vector3 playerStartPosition;
    public GameObject startDoor;

    public GameObject sceneController;
    public bool isProacherCatched;
    public float jeepFlashing;

    private bool isRightPressed;
    private bool isLeftPressed;
    private bool isUpPressed;
    private bool isDownPressed;
    private bool isPlayerInScene;
    private bool isStarted;

    // Use this for initialization
    void Start() {
      //  player.transform.localPosition = playerStartPosition;
        isRightPressed = false;
        isLeftPressed = false;
        isUpPressed = false;
        isDownPressed = false;
        isPlayerInScene = false;
        isStarted = false;
        isProacherCatched = false;
    }


    // Update is called once per frame
    void Update() {

        Vector3 tryMove = Vector3.zero;
        if (!isPlayerInScene && isStarted) {
            speed = 2;
            tryMove += Vector3Int.right;
        } else {
            
            speed = 4;
            if (isRightPressed) {
                tryMove += Vector3Int.right;
            } else if (isLeftPressed) {
                tryMove += Vector3Int.left;
            }
            if (isUpPressed) {
                tryMove += Vector3.up;
            } else if (isDownPressed) {
                tryMove += Vector3.down;
            }
            if (isRightPressed || isLeftPressed || isUpPressed || isDownPressed){
                CancelInvoke("JeepFlashing");
            }
        }
        playerRB.velocity = Vector3.ClampMagnitude(tryMove, 1f) * speed;
    }

    void JeepFlashing()
    {
        if (player.activeSelf)
            player.SetActive(false);
        else
            player.SetActive(true);
    }

    #region player_controls
    //*** Player Control Buttons Events

    public void UpButtonPressed() {
        isUpPressed = !isDownPressed;
    }

    public void RightButtonPressed() {
        isRightPressed = !isLeftPressed;
    }

    public void DownButtonPressed() {
        isDownPressed = !isUpPressed;
    }

    public void LeftButtonPressed() {
        isLeftPressed = !isRightPressed;
    }

    public void UpButtonUp() {
        isUpPressed = false;
    }

    public void RightButtonUp() {
        isRightPressed = false;
    }

    public void DownButtonUp() {
        isDownPressed = false;
    }

    public void LeftButtonUp() {
        isLeftPressed = false;
    }
    #endregion

    void OnCollisionEnter2D(Collision2D col) {
        
        if (col.gameObject.name == "StartPositionController") {
            col.gameObject.SetActive(false);
            startDoor.SetActive(true);
            isPlayerInScene = true;
        }
        if (col.gameObject.name == "EndPositionController" && isProacherCatched) {
            Debug.Log("Finish");
            sceneController.GetComponent<SafariSceneController>().Stepper();
        }
       
    }
    private void OnTriggerEnter2D(Collider2D col) {
        if(col.gameObject.tag == "animal") {
            col.gameObject.GetComponent<AnimalTagControl>().Tagged();

        }
        if (col.gameObject.name == "ProacherController") {
            sceneController.GetComponent<SafariSceneController>().PoacherInAnim();
        }
        if(col.gameObject.name == "poacher") {
            isProacherCatched = true;
            sceneController.GetComponent<SafariSceneController>().CatchProacher();
        }
    }

    public void GetPlayerToScene() {
        isStarted = true;
        InvokeRepeating("JeepFlashing", 0, jeepFlashing);
    }
}
