﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NHSPlayerController : MonoBehaviour {

    public GameObject player;
    public Rigidbody2D playerRB;
    public float speed;

    public GameObject sceneController;
    public GameObject[] walls;

    public GameObject[] cards;

    private bool isRightPressed;
    private bool isLeftPressed;
    private bool isUpPressed;
    private bool isDownPressed;
    private bool isPlayerInScene;
    private bool isStarted;
    private NHSSceneController NHSSceneContol;

    // Use this for initialization
    void Start () {
        isRightPressed = false;
        isLeftPressed = false;
        isUpPressed = false;
        isDownPressed = false;
        isPlayerInScene = false;
        isStarted = false;
        NHSSceneContol = sceneController.GetComponent<NHSSceneController>();
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 tryMove = Vector3.zero;
        if (!isPlayerInScene && isStarted) {
            speed = 2;
            tryMove += Vector3Int.right;

        } else {
           // Debug.Log("kakakakakakakakak");
            speed = 4;
            if (isRightPressed) {
                NHSSceneContol.CancelFlashPlayer();
                 tryMove += Vector3Int.right;
            } else if (isLeftPressed) {
                NHSSceneContol.CancelFlashPlayer();
                tryMove += Vector3Int.left;
            }

            if (isUpPressed) {
                NHSSceneContol.CancelFlashPlayer();
                tryMove += Vector3.up;
            } else if (isDownPressed) {
                NHSSceneContol.CancelFlashPlayer();
                tryMove += Vector3.down;
            }
        }
        playerRB.velocity = Vector3.ClampMagnitude(tryMove, 1f) * speed;
    }

    //  0 -> A
    //  1 -> 

    void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.name == "StartPositionController") {
            col.gameObject.SetActive(false);
            isPlayerInScene = true;
            for (int i = 0; i < walls.Length; i++){
                walls[i].SetActive(true);
            }
        }

       

    }

    private void OnTriggerEnter2D(Collider2D col) {
     
        Debug.Log(">>>>>>>>>>>>>>> :: " + col.gameObject.name);

        if (col.gameObject.name == "A" && NHSSceneContol.selectedCard == 0) {
            NHSSceneContol.RightPatient(0);
            CloseCard(0);
        } else if (col.gameObject.name == "A" && NHSSceneContol.selectedCard != 0) {
            NHSSceneContol.WrongPatient();
        }

        if (col.gameObject.name == "B" && NHSSceneContol.selectedCard == 1) {
            NHSSceneContol.RightPatient(1);
            CloseCard(1);
        } else if (col.gameObject.name == "B" && NHSSceneContol.selectedCard != 1) {
            NHSSceneContol.WrongPatient();
        }

        if (col.gameObject.name == "C" && NHSSceneContol.selectedCard == 2) {
            NHSSceneContol.RightPatient(2);
            CloseCard(2);
        } else if (col.gameObject.name == "C" && NHSSceneContol.selectedCard != 2) {
            NHSSceneContol.WrongPatient();
        }

        if (col.gameObject.name == "D" && NHSSceneContol.selectedCard == 3) {
            NHSSceneContol.RightPatient(3);
            CloseCard(3);
        } else if (col.gameObject.name == "D" && NHSSceneContol.selectedCard != 3) {
            NHSSceneContol.WrongPatient();
        }


        isUpPressed = false;
        isRightPressed = false;
        isDownPressed = false;
        isLeftPressed = false;
    }

    #region player_controls
    //*** Player Control Buttons Events

    public void UpButtonPressed() {
        Debug.Log("UP PRESSED");
        isUpPressed = !isDownPressed;
    }

    public void RightButtonPressed() {
        isRightPressed = !isLeftPressed;
    }

    public void DownButtonPressed() {
        isDownPressed = !isUpPressed;
    }

    public void LeftButtonPressed() {
        isLeftPressed = !isRightPressed;
    }

    public void UpButtonUp() {
        isUpPressed = false;
    }

    public void RightButtonUp() {
        isRightPressed = false;
    }

    public void DownButtonUp() {
        isDownPressed = false;
    }

    public void LeftButtonUp() {
        isLeftPressed = false;
    }
    #endregion

    public void GetPlayerToScene() {
        isStarted = true;
    }

    public void OpenCard(int card) {
        cards[card].SetActive(true);
    }

    private void CloseCard(int card) {
        cards[card].SetActive(false);
    }
}
