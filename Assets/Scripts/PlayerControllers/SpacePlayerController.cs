﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpacePlayerController : MonoBehaviour {


    public Rigidbody2D playerRB;
    public GameObject sceneController;
    public float speed;


    private bool isRightPressed;
    private bool isLeftPressed;
    private bool isUpPressed;
    private bool isDownPressed;
    private bool isPlayerInScene;
    private bool isStarted;


    // Use this for initialization
    void Start () {

        isRightPressed = false;
        isLeftPressed = false;
        isUpPressed = false;
        isDownPressed = false;
        isPlayerInScene = false;
        isStarted = false;
    }
	
	// Update is called once per frame
	void Update () {
       
        Vector3 tryMove = Vector3.zero;
        if (!isPlayerInScene && isStarted) {
            speed = 2;
            tryMove += Vector3Int.right;
        } else {
            speed = 4;
            if (isRightPressed) {
                tryMove += Vector3Int.right;
            } else if (isLeftPressed) {
                tryMove += Vector3Int.left;
            }

            if (isUpPressed) {
                tryMove += Vector3.up;
            } else if (isDownPressed) {
                tryMove += Vector3.down;
            }
            if (isRightPressed || isLeftPressed || isUpPressed || isDownPressed){
                sceneController.GetComponent<SpaceSceneController>().StopSpaceFlashing();
            }
        }
        playerRB.velocity = Vector3.ClampMagnitude(tryMove, 1f) * speed;
    }

    void OnCollisionEnter2D(Collision2D col) {

        if (col.gameObject.name == "FinishPositionController") {
            SoundManager.instance.StopSound("Thrustler");
            sceneController.GetComponent<SpaceSceneController>().Stepper();
        }
        if(col.gameObject.tag == "Hole") {
            SoundManager.instance.StopSound("Thrustler");
            sceneController.GetComponent<SpaceSceneController>().HitWallErrorMessage();
            isRightPressed = false;
            isLeftPressed = false;
            isUpPressed = false;
            isDownPressed = false;
            isPlayerInScene = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D col) {

        if(col.gameObject.name == "ShootingStars") {
            sceneController.GetComponent<SpaceSceneController>().TurnOffHoles();
            SoundManager.instance.PlaySound("StarSound");
        }
    }



    #region player_controls
    //*** Player Control Buttons Events

    public void UpButtonPressed() {
        SoundManager.instance.PlaySound("Thrustler");
        isUpPressed = !isDownPressed;
    }

    public void RightButtonPressed() {
        SoundManager.instance.PlaySound("Thrustler");
        isRightPressed = !isLeftPressed;
    }

    public void DownButtonPressed() {
        SoundManager.instance.PlaySound("Thrustler");
        isDownPressed = !isUpPressed;
    }

    public void LeftButtonPressed() {
        SoundManager.instance.PlaySound("Thrustler");
        isLeftPressed = !isRightPressed;
    }

    public void UpButtonUp() {
        SoundManager.instance.StopSound("Thrustler");
        isUpPressed = false;
    }

    public void RightButtonUp() {
        SoundManager.instance.StopSound("Thrustler");
        isRightPressed = false;
    }

    public void DownButtonUp() {
        SoundManager.instance.StopSound("Thrustler");
        isDownPressed = false;
    }

    public void LeftButtonUp() {
        SoundManager.instance.StopSound("Thrustler");
        isLeftPressed = false;
    }
    #endregion
}
