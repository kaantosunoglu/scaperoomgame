﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{
    public static VideoManager instance;

    [SerializeField]
    public MyVideoClip[] videos;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            return;
        }
    }

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < videos.Length; i++)
        {
            GameObject videoGameObject = new GameObject("Video" + i + "_" + videos[i].name);
            videos[i].SetVideoSource(videoGameObject.AddComponent<VideoPlayer>());
        }
    }

    public VideoPlayer PlayVideo(string name)
    {
        for (int i = 0; i < videos.Length; i++)
        {
            if (name == videos[i].name)
            {
                return videos[i].Play();
            }
        }

        return videos[0].Play();
    }

    // Update is called once per frame
    void Update()
    {

    }

}
