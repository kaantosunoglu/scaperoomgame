﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[System.Serializable]
public class MyVideoClip {
    private VideoPlayer videoPlayer;
    private Camera mainCamera;

    public VideoClip videoClip;
    public string name;
    public VideoAudioOutputMode audioOutput = VideoAudioOutputMode.Direct;

    public void SetVideoSource(VideoPlayer vp)
    {
        mainCamera = Camera.main;
        videoPlayer = vp;
        videoPlayer.playOnAwake = false;
        videoPlayer.isLooping = false;
        videoPlayer.renderMode = VideoRenderMode.CameraFarPlane;
        videoPlayer.audioOutputMode = audioOutput;
        videoPlayer.targetCamera = mainCamera;
        videoPlayer.clip = videoClip;
    }

    public VideoPlayer Play()
    {
        videoPlayer.Play();
        return videoPlayer;
    }
}
