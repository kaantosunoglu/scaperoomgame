﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound {


    public enum EarSelection {
        Left,
        Both,
        Right
    };
    public string name;
    public AudioClip audioClip;
    public bool loop = false;
    [Range(0f, 1.0f)]
    public float soundVolume = 1f;

    public EarSelection earSelection = EarSelection.Both;
    private AudioSource audioSource;


    public void SetAudioSource(AudioSource source) {
        audioSource = source;
        audioSource.clip = audioClip;
        audioSource.loop = loop;
        audioSource.playOnAwake = false;
        audioSource.panStereo = (float)(earSelection - 1);
        audioSource.volume = soundVolume;
    }

    public void Play() {
        audioSource.Play();
    }
	
    public void Stop() {
        audioSource.Stop();
    }

    public void Pause(){
        audioSource.Pause();
    }

    public void UnPause() {
        audioSource.UnPause();
    }

}
