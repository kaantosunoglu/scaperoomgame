﻿//    Example: Calculator

//    Somewhat dummy example code. Main thing is actual display, showing how it is simple to give LCD look for display by adding gray background
//    and setting segment display "On Color" to black or dark gray.


using UnityEngine;
using Leguar.SegmentDisplay;

public class Example_Calculator : MonoBehaviour {
	
	private SegmentDisplay display;

	private string currentInput;
	private float timer;

	void Start() {

		// Just another way to get reference to display
		display = GameObject.Find("SegmentDisplay_Calc").GetComponent<SegmentDisplay>();

		currentInput = "";
		timer = 0f;

	}
	
	void Update() {

		if (display.GetSDController().IsIdle()) { // Check that display isn't showing "-E-" message at the moment
			timer+=Time.deltaTime;
			if (timer>1f) {
				if (currentInput.Length==0) {
					if (Random.value<0.8f) {
						currentInput=""+Random.Range(1,10);
					} else {
						currentInput="0.";
					}
				} else {
					if (currentInput.IndexOf('.')>=0 || Random.value<0.9f) {
						currentInput += ""+Random.Range(0,10);
					} else {
						currentInput += ".";
					}
				}
				int inputDigitLength=currentInput.Replace(".","").Length;
				if (inputDigitLength<=display.DigitCount) {
					display.SetText(currentInput);
				} else {
					// Well, calculators do not usually go to error state when typing too many digits, but this one is special
					display.SetText("-E-",SegmentDisplay.Alignments.Left);
					display.GetSDController().AddCommand(new PauseCommand(3f));
					display.GetSDController().AddCommand(new SetTextCommand("0"));
					currentInput="";
				}
				timer=0f;
			}
		}

	}
	
}
