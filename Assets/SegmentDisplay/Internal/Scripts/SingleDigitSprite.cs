﻿//    Class for sprite based segment digit


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Leguar.SegmentDisplay {

	public class SingleDigitSprite : SingleDigit {

		public SpriteRenderer[] segmentSprites;
		public SpriteRenderer decimalPointSprite;
		public SpriteRenderer colonLowerSprite;
		public SpriteRenderer colonUpperSprite;

		public override int BaseSegmentCount {
			get {
				return segmentSprites.Length;
			}
		}

		internal override void createSegments() {

            SegmentDisplaySprite parentSpriteDisplay = (SegmentDisplaySprite)(base.parentDisplay);

			if (mode!=Mode.Colon) {
				for (int n=0; n<BaseSegmentCount; n++) {
                    segments[n]=new SingleSegmentSprite(parentSpriteDisplay,segmentSprites[n]);
				}
			} else {
				for (int n=0; n<BaseSegmentCount; n++) {
					segmentSprites[n].gameObject.SetActive(false);
				}
			}
			
			if (mode==Mode.DigitDp) {
                segments[BaseSegmentCount]=new SingleSegmentSprite(parentSpriteDisplay,decimalPointSprite);
			} else {
				decimalPointSprite.gameObject.SetActive(false);
			}
			
			if (mode==Mode.Colon) {
                segments[0]=new SingleSegmentSprite(parentSpriteDisplay,colonLowerSprite);
                segments[1]=new SingleSegmentSprite(parentSpriteDisplay,colonUpperSprite);
			} else {
				colonLowerSprite.gameObject.SetActive(false);
				colonUpperSprite.gameObject.SetActive(false);
			}

		}

		internal void spread(float m) {
			foreach (SingleSegment segment in segments) {
				((SingleSegmentSprite)(segment)).multiplyPosition(m);
			}
		}

        internal void doSpriteOrderRefresh() {
            int sortingOrder = ((SegmentDisplaySprite)(base.parentDisplay)).SpriteSortingOrder;
            foreach (SingleSegment segment in segments) {
                ((SingleSegmentSprite)(segment)).setSortingOrder(sortingOrder);
            }
        }
    
    }

}
