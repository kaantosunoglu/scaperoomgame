﻿//    Class for image based segment digit


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Leguar.SegmentDisplay {

	public class SingleDigitImage : SingleDigit {

		public Image[] segmentImages;
		public Image decimalPointImage;
		public Image colonLowerImage;
		public Image colonUpperImage;

		public override int BaseSegmentCount {
			get {
				return segmentImages.Length;
			}
		}

		internal override void createSegments() {

			if (mode!=Mode.Colon) {
				for (int n=0; n<BaseSegmentCount; n++) {
					segments[n]=new SingleSegmentImage(parentDisplay,segmentImages[n]);
				}
			} else {
				for (int n=0; n<BaseSegmentCount; n++) {
					segmentImages[n].gameObject.SetActive(false);
				}
			}
			
			if (mode==Mode.DigitDp) {
				segments[BaseSegmentCount]=new SingleSegmentImage(parentDisplay,decimalPointImage);
			} else {
				decimalPointImage.gameObject.SetActive(false);
			}
			
			if (mode==Mode.Colon) {
				segments[0]=new SingleSegmentImage(parentDisplay,colonLowerImage);
				segments[1]=new SingleSegmentImage(parentDisplay,colonUpperImage);
			} else {
				colonLowerImage.gameObject.SetActive(false);
				colonUpperImage.gameObject.SetActive(false);
			}

		}

		internal void spreadAndMultiplySize(float spread, float multipX, float multipY) {
			foreach (SingleSegment segment in segments) {
				((SingleSegmentImage)(segment)).spreadAndMultiplySize(spread,multipX,multipY);
			}
		}

	}

}
