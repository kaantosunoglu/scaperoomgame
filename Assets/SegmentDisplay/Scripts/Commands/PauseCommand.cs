﻿//    SegmentDisplay - Controller - Command - Pause


namespace Leguar.SegmentDisplay {
	
	/// <summary>
	/// Command to pause executing commands until certain amount of time is passed. Whatever is on display when pause is reached will stay there.
	/// </summary>
	public class PauseCommand : AbsSDCommand {
		
		private float pauseSeconds;
		
		/// <summary>
		/// Creates new pause command.
		/// </summary>
		/// <param name="pauseSeconds">
		/// Seconds how long SDController will be paused.
		/// </param>
		public PauseCommand(float pauseSeconds) {
			this.pauseSeconds=pauseSeconds;
		}
		
		internal override RawCmd getRawCommand() {
			return (new RawCmdDelay(pauseSeconds));
		}
		
	}

}
